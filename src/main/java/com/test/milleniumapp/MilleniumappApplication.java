package com.test.milleniumapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MilleniumappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MilleniumappApplication.class, args);
	}

}
