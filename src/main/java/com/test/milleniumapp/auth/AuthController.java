package com.test.milleniumapp.auth;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microsoft.aad.msal4j.IAuthenticationResult;
import com.nimbusds.jwt.JWTParser;

@RestController
@CrossOrigin
public class AuthController {

	
	private boolean authenticated = false;
	
	@Autowired
	AuthHelper authHelper;
	
	/**
	 * Endpoint que hace el login del usuario por primera vez
	 * @param request
	 * @return JSON con mensaje que el usuario ha sido autenticado
	 * @throws ParseException
	 */
	@RequestMapping("/info")
	public Map<String, Object> getInfo(HttpServletRequest request) throws ParseException {
		
		
		String authResult = authenticateUser(request);
		
		HashMap<String, Object> response = new HashMap<>();
		response.put("status", "ok");
		response.put("info","authentication successful");
		
		return response;
	}
	
	
	/**
	 * endpoint que permite al frontend verificar si el usuario esta autenticado o no
	 * @return
	 */
	@RequestMapping("/isAuthenticated")
	public Map<String, Object> isAuthenticated() {
		
		HashMap<String, Object> response = new HashMap<>();
		response.put("authenticated", authenticated);
		return response;
		
	}
	
	/**
	 * Funcion que valida que el tenantID del usuario autenticado pertenzca a credicomer o unicomer
	 * Si pertenece, el valor de la bandera authenticated pasa a ser True;
	 * @param request
	 * @return El token si pertenece a una de las empresas, "unauthorized" si no esta autorizado
	 * @throws ParseException
	 */
	String authenticateUser(HttpServletRequest request) throws ParseException {
		
		IAuthenticationResult auth = SessionManagementHelper.getAuthSessionObject(request);
		
		
		String tenantId = JWTParser.parse(auth.idToken()).getJWTClaimsSet().getStringClaim("tid");
		
		//Valida si el tenant pertenece a unicomer o credicomer
		if(AuthHelper.authorizedTenants.contains(tenantId)) {
			authenticated = true;
			return auth.idToken();
		}
		return "unauthorized";
		
	}
	
}
